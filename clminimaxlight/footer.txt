  <th1>
    if {[string first artifact $current_page] == 0 || [string first hexdump $current_page] == 0} {
      html "</div>"
    }
  </th1>
  </div> <!-- end div container -->
</div> <!-- end div middle max-full-width -->
<div class="footer">
  <div class="container">
    <div class="pull-right">
      <a href="http://hyscm.hyang.org/cgi-bin/hyscm-dev/hyscm">HySCM $release_version $hyscmterm_version $hyscmterm_date</a>
    </div>
    Copyright (C) 2019 The Hyang Language Foundation, Jakarta. Skin theme by <a href="https://gitlab.com/novary/hyscm-skin">Novary</a>. This page was generated in about <th1>puts [expr {([utime]+[stime]+1000)/1000*0.001}]</th1>s
  </div>
</div>
