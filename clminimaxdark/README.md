## The cLMiniMaxDark Theme version 1.2 (24 Nov 2019) for HySCM

This theme is developed by [Yana Novarizal](https://gitlab.com/novary/hyscm-skin), licensed under MIT license.

This theme uses icons for the navigations and the WYSIWYG editor, which are embedded in the css as base64.

This theme uses a modified copy of [Normalize 3.0.2](https://necolas.github.io/normalize.css/) and [Skeleton](http://getskeleton.com), both are distributed under MIT license.
