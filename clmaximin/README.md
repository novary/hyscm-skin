## The cLMaxiMin Theme version 1.0 for HySCM

This theme uses modified copies of HySCM default theme, with new updated css.
This theme is developed by [Yana Novarizal](https://gitlab.com/novary/hyscm-skin),
licensed under MIT license.

