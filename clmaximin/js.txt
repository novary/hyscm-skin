/*
** Copyright (C) 2019 The Hyang Language Foundation, Jakarta.
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the Simplified BSD License (also
** known as the "2-Clause License" or "FreeBSD License".)
**
** This program is distributed in the hope that it will be useful,
** but without any warranty; without even the implied warranty of
** merchantability or fitness for a particular purpose.
**
**
*********************************************************************
**
** This file contains the JS code specific to the HySCM default theme.
*/

(function() {
  var hbButton = document.getElementById("hbbtn");
  if (!hbButton) return;   // no hamburger button
  if (!document.addEventListener) {
    hbButton.href = "$home/sitemap";
    return;
  }
  var panel = document.getElementById("hbdrop");
  if (!panel) return;
  if (!panel.style) return;
  var panelBorder = panel.style.border;
  var panelInitialized = false; 
  var panelResetBorderTimerID = 0; 
  var animate = panel.style.transition !== null && (typeof(panel.style.transition) == "string");

  var animMS = panel.getAttribute("data-anim-ms");
  if (animMS) {
    animMS = parseInt(animMS);
    if (isNaN(animMS) || animMS == 0)
      animate = false; 
    else if (animMS < 0)
      animMS = 400;
  }
  else
    animMS = 400;

  var panelHeight;
  function calculatePanelHeight() {

    panel.style.maxHeight = '';

    var es   = window.getComputedStyle(panel),
        edis = es.display,
        epos = es.position,
        evis = es.visibility;

    panel.style.visibility = 'hidden';
    panel.style.position   = 'absolute';
    panel.style.display    = 'block';
    panelHeight = panel.offsetHeight + 'px';

    panel.style.display    = edis;
    panel.style.position   = epos;
    panel.style.visibility = evis;
  }

  function showPanel() {
    if (panelResetBorderTimerID) {
      clearTimeout(panelResetBorderTimerID);
      panelResetBorderTimerID = 0;
    }
    if (animate) {
      if (!panelInitialized) {
        panelInitialized = true;
        calculatePanelHeight();
        panel.style.transition = 'max-height ' + animMS +
            'ms ease-in-out';
        panel.style.overflowY  = 'hidden';
        panel.style.maxHeight  = '0';
      }
      setTimeout(function() {
        panel.style.maxHeight = panelHeight;
        panel.style.border    = panelBorder;
      }, 40);
    }
    panel.style.display = 'block';
    document.addEventListener('keydown',panelKeydown,/* useCapture == */true);
    document.addEventListener('click',panelClick,false);
  }

  var panelKeydown = function(event) {
    var key = event.which || event.keyCode;
    if (key == 27) {
      event.stopPropagation();
      panelToggle(true);
    }
  };

  var panelClick = function(event) {
    if (!panel.contains(event.target)) {
      panelToggle(true);
    }
  };

  function panelShowing() {
    if (animate) {
      return panel.style.maxHeight == panelHeight;
    }
    else {
      return panel.style.display == 'block';
    }
  }

  function hasChildren(element) {
    var childElement = element.firstChild;
    while (childElement) {
      if (childElement.nodeType == 1) // Node.ELEMENT_NODE == 1
        return true;
      childElement = childElement.nextSibling;
    }
    return false;
  }

  window.addEventListener('resize',function(event) {
    panelInitialized = false;
  },false);

  hbButton.addEventListener('click',function(event) {
    event.stopPropagation();
    event.preventDefault();
    panelToggle(false);
  },false);

  function panelToggle(suppressAnimation) {
    if (panelShowing()) {
      document.removeEventListener('keydown',panelKeydown,/* useCapture == */true);
      document.removeEventListener('click',panelClick,false);
      if (animate) {
        if (suppressAnimation) {
          var transition = panel.style.transition;
          panel.style.transition = '';
          panel.style.maxHeight = '0';
          panel.style.border = 'none';
          setTimeout(function() {
            panel.style.transition = transition;
          }, 40);
        }
        else {
          panel.style.maxHeight = '0';
          panelResetBorderTimerID = setTimeout(function() {
            panel.style.border = 'none';
            panelResetBorderTimerID = 0;
          }, animMS);
        }
      }
      else {
        panel.style.display = 'none';
      }
    }
    else {
      if (!hasChildren(panel)) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
          var doc = xhr.responseXML;
          if (doc) {
            var sm = doc.querySelector("ul#sitemap");
            if (sm && xhr.status == 200) {
              panel.innerHTML = sm.outerHTML;
              showPanel();
            }
          }
        }
        xhr.open("GET", "$home/sitemap?popup"); 
        xhr.responseType = "document";
        xhr.send();
      }
      else {
        showPanel();
      }
    }
  }
})();
