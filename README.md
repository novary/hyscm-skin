Tema Repositori HySCM
=====================

   1. cLMiniMax : ./clminimax/README.md
   2. cLMiniMaxDark : ./clminimaxdark/README.md
   3. cLMiniMaxLight : ./clminimaxlight/README.md

Ketiganya didistribusi dengan lisensi MIT.


Petunjuk Pengembangan
---------------------

Salin direktori tema yang wajib berisi berkas css.txt, details.txt, footer.txt, dan
header.txt ke direktori kerja repo lokal HySCM. Luncurkan hyscm dengan opsi baris-perintah
"--skin ./namatema".  Misalnya:

        hyscm ui --skin ./clminimax
        hyscm ui --skin ./clminimaxdark
        hyscm ui --skin ./clminimaxlight

Lalu tekan CTRL+C untuk keluar dari UI web, dan edit css.txt, dst.

Email untuk laporan dan lain-lain : <yananovarizal@gmail.com>.
